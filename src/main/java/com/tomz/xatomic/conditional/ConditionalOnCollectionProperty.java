package com.tomz.xatomic.conditional;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnCollectionPropertyCondition.class)
public @interface ConditionalOnCollectionProperty {

    /**
     * key property prefix
     * @return
     */
    String value();

}
