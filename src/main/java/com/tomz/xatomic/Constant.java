package com.tomz.xatomic;

import org.apache.ibatis.session.Configuration;

/**
 * @author ZHUFEIFEI
 */
public class Constant {
    public static final String DATA_SOURCE_PROPERTY_PREFIX = "spring.datasources";
    /**
     * {@link org.springframework.boot.jta.atomikos.AtomikosDataSourceBean}
     */
    public static final String XA_DATA_SOURCE_PROPERTY_PREFIX = "spring.jta.atomikos.datasource";
    public static final String DATA_SOURCE_DEFAULT_KEY = "default";
    public static final String DATA_SOURCE_BEAN_NAME = "dynamicDataSource";
    /**
     * {@link org.mybatis.spring.boot.autoconfigure.MybatisProperties#setConfiguration(Configuration)}
     */
    public static final String MYBATIS_CONFIG_PREFIX = "mybatis.configuration";
}
